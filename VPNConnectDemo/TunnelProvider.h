//
//  TunnelProvider.h
//  VPNConnectDemo
//
//  Created by Andolasoft on 2/24/17.
//  Copyright © 2017 Andola. All rights reserved.
//

#import <NetworkExtension/NetworkExtension.h>

@interface TunnelProvider : NEPacketTunnelProvider

- (void)startTunnelWithOptions:(NSDictionary *)options completionHandler:(void (^)(NSError *))completionHandler;

- (void)stopTunnelWithReason:(NEProviderStopReason)reason completionHandler:(void (^)(void))completionHandler;

@end
