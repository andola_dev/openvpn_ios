//
//  ViewController.m
//  VPNConnectDemo
//
//  Created by Andolasoft on 2/23/17.
//  Copyright © 2017 Andola. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self configureVPN];
}

- (void)configureVPN
{
    //Setup the NETunnelProviderProtocol object with appropriate values. The most important field is providerBundleIdentifier, which MUST be the bundle ID of our custom network extension target.
    
    [NETunnelProviderManager loadAllFromPreferencesWithCompletionHandler:^(NSArray<NETunnelProviderManager *> * _Nullable managers, NSError * _Nullable error) {
        if (error) {
            NSLog(@"Load Error: %@", error.description);
        }
        
        NETunnelProviderManager *manager;
        if (managers.count > 0) {
            manager = managers[0];
        }else {
            manager = [[NETunnelProviderManager alloc] init];
            
            NETunnelProviderProtocol *protocol = [[NETunnelProviderProtocol alloc] init];
            
            protocol.providerBundleIdentifier = @"com.vpndemo1";	// bundle ID of tunnel provider
            
            protocol.providerConfiguration = @{@"username":@"openvpn", @"password":@"andola"};

            protocol.serverAddress = @"111.93.179.8";		// VPN server address
            manager.protocolConfiguration = protocol;
            
            [manager saveToPreferencesWithCompletionHandler:^(NSError *error) {
                if (error) {
                    NSLog(@"Error 1: %@", error.description);
                } else {
                    [manager loadFromPreferencesWithCompletionHandler:^(NSError * _Nullable error) {
                        if (error) {
                            NSLog(@"Error 2: %@", error.description);
                        } else {
                            [self start:manager];
                        }
                    }];
                }
            }];
        }
        
        //... your code here...
    }];
}

- (void)start:(NETunnelProviderManager *)manager
{
    NETunnelProviderSession *session = (NETunnelProviderSession*) manager.connection;
    NSDictionary *options = @{@"username":@"openvpn", @"password":@"andola"};		// Send additional options to the tunnel provider
    NSError *err;
    if([session startTunnelWithOptions:options andReturnError:&err]){
        NSLog(@" started ");
        NSLog(@"status : %ld", (long)manager.connection.status);
    }else{
        NSLog(@"Error : %@", err.localizedDescription);
    }
}

/*
- (void)sucketcommunicationWithVPN{
    NWHostEndpoint *peer = [NWHostEndpoint endpointWithHostname:@"111.93.179.8" port:@"8080"];
    
    NWUDPSession *udpSession = [self createUDPSessionToEndpoint:peer fromEndpoint:nil]];
    [udpSession writeDatagram:packet completionHandler:^(NSError * _Nullable error) {
    }];
    
    [udpSession
     setReadHandler:(void (^)(NSArray<NSData *> *datagrams,
                              NSError *error))handler
     maxDatagrams:(NSUInteger)maxDatagrams
     ]
    
    
    NSArray *addresses = @["10.0.1.100"];
    NSArray *subnetMasks = @[@"255.255.255.0"];
    NSArray<NSString *> *dnsServers = @[@"8.8.8.8", @"8.8.4.4"];
    
    NEPacketTunnelNetworkSettings *settings = [[NEPacketTunnelNetworkSettings alloc] initWithTunnelRemoteAddress:@"111.93.179.8"];
    settings.IPv4Settings = [[NEIPv4Settings alloc] initWithAddresses:addresses subnetMasks:subnetMasks];
    
    NEIPv4Route *defaultRoute = [NEIPv4Route defaultRoute];
    NEIPv4Route *localRoute = [[NEIPv4Route alloc] initWithDestinationAddress:@"10.0.0.0" subnetMask:@"255.255.255.0"];
    settings.IPv4Settings.includedRoutes = @[defaultRoute, localRoute];
    settings.IPv4Settings.excludedRoutes = @[];
    settings.MTU = [NSNumber numberWithInt:1500];
    [self setTunnelNetworkSettings:settings completionHandler:^(NSError * _Nullable error) {
        if (error) {
            NSLog(@"setTunnelNetworkSettings error: %@", error);
        } else {
            NSError *err;
            pendingCompletionHandler(err);
        }];
}
*/
     
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
