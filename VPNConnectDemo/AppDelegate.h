//
//  AppDelegate.h
//  VPNConnectDemo
//
//  Created by Andolasoft on 2/23/17.
//  Copyright © 2017 Andola. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

